package main.java.com.project.incomes.view;

import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;

import main.java.com.project.incomes.managed.bean.IncomeManagedBean;

public class IncomeBeanValueChange implements ValueChangeListener{

	public void processValueChange(ValueChangeEvent event)
			throws AbortProcessingException {
		IncomeManagedBean incomeBean = (IncomeManagedBean) FacesContext
										.getCurrentInstance()
										.getExternalContext()
										.getSessionMap().get("incomeMB");
		incomeBean.setUserId(Long.parseLong(event.getNewValue().toString()));
		
	}

}
