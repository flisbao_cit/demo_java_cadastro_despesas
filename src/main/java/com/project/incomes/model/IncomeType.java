package main.java.com.project.incomes.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;
@Entity
@Table(name="INCOME_TYPE")
public class IncomeType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int income_type_id;
	private String type;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	//@ManyToOne(fetch = FetchType.LAZY)
	@Column(name="INCOME_TYPE_ID", unique=true,nullable=false)
	public int getIncome_type_id() {
		return income_type_id;
	}
	public void setIncome_type_id(int income_type_id) {
		this.income_type_id = income_type_id;
	}
	@Column(name="TYPE",nullable=false)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
