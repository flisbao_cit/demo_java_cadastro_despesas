package main.java.com.project.incomes.model;

import java.io.Serializable;
import java.util.Date;



import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.persistence.Transient;
@Entity
@Table(name="INCOME")
public class Income implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long incomeId;
	private int incomeTypeId;
	private long userId;
	private double value;
	private Date date;
	//@Transient
	private String userName;
	//@Transient
	private String incomeTypeDesc;
	private boolean editable;
	
		

	@Id
	@Column(name="INCOME_ID", unique=true,nullable=false)
	public long getIncomeId() {
		return incomeId;
	}
	public void setIncomeId(long incomeId) {
		this.incomeId = incomeId;
	}
	
	//@OneToMany(fetch=FetchType.LAZY,mappedBy="INCOME")
	
	@Column(name="TYPE_ID",nullable=false)
	public int getIncomeTypeId() {
		return incomeTypeId;
	}
	public void setIncomeTypeId(int incomeTypeId) {
		this.incomeTypeId = incomeTypeId;
	}
	//@ManyToOne(fetch=FetchType.LAZY)
	//@JoinColumn(name="USER_ID", nullable = false)
	
	@Column(name="USER_ID",nullable=false)
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name="VALUE",nullable=false)
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
	
	@Column(name="DATE",nullable=false)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Transient
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Transient
	public String getIncomeTypeDesc() {
		return incomeTypeDesc;
	}
	public void setIncomeTypeDesc(String incomeTypeDesc) {
		this.incomeTypeDesc = incomeTypeDesc;
	}
	@Transient
	public boolean isEditable() {
		return editable;
	}
	public void setEditable(boolean editable) {
		this.editable = editable;
	}
	
	/*@Override
	public String toString(){
		StringBuffer strBuff = new StringBuffer();
		strBuff.append("id : ").append(get);
		strBuff.append(", name : ").append(getName());
		strBuff.append(", email : ").append(getEmail());
		return strBuff.toString();
	}*/
	
	
}
