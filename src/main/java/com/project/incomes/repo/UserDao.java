package main.java.com.project.incomes.repo;

import java.util.List;

import main.java.com.project.incomes.model.User;

public interface UserDao {

	 /**
	  * Add User
	  *
	  * @param  User user
	  */
	 public void addUser(User user);

	 /**
	  * Update User
	  *
	  * @param  User user
	  */
	 public void updateUser(User user);

	 /**
	  * Delete User
	  *
	  * @param  User user
	  */
	 public void deleteUser(User user);

	 /**
	  * Get User
	  *
	  * @param  int User Id
	  */
	 public User getUserById(int id);

	 /**
	  * Get User List
	  *
	  */
	 public List<User> getUsers();
	 
	 public List<User> getUsersByNameAndEmail(String name, String email);
}
