package main.java.com.project.incomes.repo.impl;


import java.util.List;

import main.java.com.project.incomes.model.User;
import main.java.com.project.incomes.repo.UserDao;


import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class UserDaoImpl implements UserDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addUser(User user) {
		// TODO Auto-generated method stub
		// getSessionFactory().getCurrentSession().save(user);
		getSessionFactory().getCurrentSession().save(user);

	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		getSessionFactory().getCurrentSession().update(user);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteUser(User user) {
		// TODO Auto-generated method stub
		getSessionFactory().getCurrentSession().delete(user);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public User getUserById(int id) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final List<User> list = getSessionFactory().getCurrentSession().createQuery("from User where id=?").setParameter(0, id).list();

		return (User) list.get(0);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public List<User> getUsers() {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final List<User> list = getSessionFactory().getCurrentSession().createQuery("from User").list();
		return list;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public List<User> getUsersByNameAndEmail(String name, String email) {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final List<User> list = getSessionFactory().getCurrentSession().createQuery("from User where name=? or email=?").setParameter(0, name).setParameter(1, email).list();
		return list;
	}

}
