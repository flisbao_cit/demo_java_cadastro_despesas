package main.java.com.project.incomes.repo.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import main.java.com.project.incomes.model.Income;
import main.java.com.project.incomes.repo.IncomeDao;

public class IncomeDaoImpl implements IncomeDao {

	private SessionFactory sessionFactory;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addIncome(Income income) {
		getSessionFactory().getCurrentSession().save(income);
	}
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateIncome(Income income) {
		getSessionFactory().getCurrentSession().update(income);
		
	}
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deleteIncome(Income income) {
		getSessionFactory().getCurrentSession().delete(income);
		
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public Income getIncomeById(int id) {
		@SuppressWarnings("unchecked")
		List<Income> list = getSessionFactory().getCurrentSession()
							.createQuery("from Income where income_id=?")
							.setParameter(0, id)
							.list();
		return (Income) list;
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Income> getIncomesByUser(long userId) {
		@SuppressWarnings("unchecked")
		final List<Income> list = getSessionFactory().getCurrentSession()
							.createQuery("from Income where user_id=?")
							.setParameter(0, userId)
							.list();
		return list;
	}
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Income> getIncomes() {
		// TODO Auto-generated method stub
		@SuppressWarnings("unchecked")
		final List<Income> list = getSessionFactory()
									.getCurrentSession()
									.createQuery("from Income")
									.list();
		return list;
	}
	
	public List<Income> getIncomesByDate(Date dtIni, Date dtFin, long userId) {
		// TODO Auto-generated method stub
		// Date dataInicio = new Date();
		// Date dataFim = new Date();
		try{
		//	dataInicio = DateUtils.truncate(dtIni, Calendar.DATE);
			Calendar c = Calendar.getInstance(); 
			c.setTime(dtFin); 
			c.add(Calendar.DATE, 1);
			dtFin = c.getTime();
			//dataFim = DateUtils.truncate(dtFin, Calendar.DATE);
			
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		@SuppressWarnings("unchecked")
		final List<Income> list = getSessionFactory()
									.getCurrentSession()
									.createQuery("from Income where user_id=? and date between ? and ?")
									.setParameter(0, userId)
									.setDate(1, dtIni)
									.setDate(2, dtFin)
									.list();
		return list;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
