package main.java.com.project.incomes.repo;

import java.util.List;

import main.java.com.project.incomes.model.IncomeType;



public interface IncomeTypeDao {
	
	 public IncomeType getIncomeTypeById(int id);

	 /**
	  * Get User List
	  *
	  */
	 public List<IncomeType> getIncomeTypes();

}
