package main.java.com.project.incomes.repo;

import java.util.Date;
import java.util.List;

import main.java.com.project.incomes.model.Income;


public interface IncomeDao {

	/**
	  * Add User
	  *
	  * @param  User user
	  */
	 public void addIncome(Income income);

	 /**
	  * Update User
	  *
	  * @param  User user
	  */
	 public void updateIncome(Income income);

	 /**
	  * Delete User
	  *
	  * @param  User user
	  */
	 public void deleteIncome(Income income);

	 /**
	  * Get User
	  *
	  * @param  int User Id
	  */
	 public Income getIncomeById(int id);

	 /**
	  * Get User List
	  *
	  */
	 public List<Income> getIncomesByUser(long userId);
	 public List<Income> getIncomes();
	 public List<Income> getIncomesByDate(Date dtIni, Date dtFin, long userId);
}
