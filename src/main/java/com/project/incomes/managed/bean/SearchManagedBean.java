package main.java.com.project.incomes.managed.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import main.java.com.project.incomes.model.Income;
import main.java.com.project.incomes.model.User;
import main.java.com.project.incomes.service.IncomeService;
import main.java.com.project.incomes.service.UserService;
import main.java.com.project.incomes.service.impl.BusinessException;

@ManagedBean(name = "searchMB")
@ViewScoped
public class SearchManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private long userId;
	private long incomeId;
	private int incomeType;
	private double value;
	private String userName;
	private String incomeTypeDes;
	private double totalExpenses;
	private double totalIncomes;
	private double total;
	private Date dateIni;
	private Date dateFin;
	private boolean renderPanel = false;
	private String outputLabel = "Adicionar Nova Despesa";
	private List<String> msg = new ArrayList<String>();
	private String outputMsg = "";
	private boolean successMsg = false;
	private boolean errorMsg = false;
	private List<User> listUsers;
	private Income selectIncome;
	private ResourceBundle rb = ResourceBundle.getBundle("bundle.message");

	private Income incomes;

	public Income getIncomes() {
		return incomes;
	}

	public void setIncomes(Income incomes) {
		this.incomes = incomes;
	}

	List<Income> incomeUserList;

	@ManagedProperty(value = "#{incomeServiceImpl}")
	IncomeService incomeService;
	@ManagedProperty(value = "#{userServiceImpl}")
	UserService userService;

	public String searchIncome() {
		loadIncomes();
		return "";
	}

	public void renderPanel() {
		if (!isRenderPanel()) {
			setRenderPanel(true);
			setOutputLabel("Fechar");
		} else {
			setRenderPanel(false);
			setOutputLabel("Adicionar Nova Despesa");

		}
	}

	@PostConstruct
	public void init() {
		setRenderPanel(false);
		setOutputLabel("Adicionar Nova Despesa");
		setErrorMsg(false);
		setSuccessMsg(false);
		loadUsers();
	}

	public void loadUsers() {
		listUsers = new ArrayList<User>();
		setListUsers(getUserService().getUsers());

	}

	public void loadIncomes() {

		incomeUserList = new ArrayList<Income>();
		incomeUserList.addAll(getIncomeService().getIncomesByDate(getDateIni(), getDateFin(), getUserId()));
		totalExpenses = 0;
		totalIncomes = 0;
		total = 0;
		for (Income inc : incomeUserList) {
			inc.setUserName(getUserService().getUserById((int) getUserId()).getName());
			inc.setEditable(false);
			if (inc.getIncomeTypeId() == 1) {
				inc.setIncomeTypeDesc("Entrada");
				totalIncomes = totalIncomes + inc.getValue();
			} else {
				inc.setIncomeTypeDesc("Despesa");
				totalExpenses = totalExpenses + inc.getValue();
			}

		}
		total = totalIncomes - totalExpenses;
		System.out.println(getUserId());
	}

	public void resetFields() {
		setValue(0);
		setIncomeType(-1);
	}

	public void loadIncomesAction(boolean editable, long id) {
		incomeUserList = new ArrayList<Income>();
		incomeUserList.addAll(getIncomeService().getIncomesByDate(getDateIni(), getDateFin(), getUserId()));
		totalExpenses = 0;
		totalIncomes = 0;
		total = 0;
		for (Income inc : incomeUserList) {
			inc.setUserName(getUserService().getUserById((int) getUserId()).getName());

			if (inc.getIncomeId() == id) {
				inc.setEditable(editable);
			} else {
				inc.setEditable(!editable);
			}
			if (inc.getIncomeTypeId() == 1) {
				inc.setIncomeTypeDesc("Entrada");
				//totalIncomes = totalIncomes + inc.getValue();
				totalIncomes+=inc.getValue();
			} else {
				inc.setIncomeTypeDesc("Saida");
				//totalExpenses = totalExpenses + inc.getValue();
				totalExpenses += inc.getValue();
			}

		}
		total = totalIncomes - totalExpenses;
		resetFields();

	}

	public String editAction(Income income) {

		loadIncomesAction(true, income.getIncomeId());

		return "";
	}

	public String saveAction() {
		// setIncomes(income);
		clearMsg();
		setErrorMsg(false);
		setSuccessMsg(false);
		incomes.setEditable(false);
		incomes.setDate(new Date());

		try {
			addMsg(getIncomeService().updateIncome(getIncomes()));
			setSuccessMsg(true);
		} catch (BusinessException e) {
			setMsg(e.getMsg());
			setErrorMsg(true);
		}

		loadIncomes();

		return "";
	}

	public String addIncome() {
		clearMsg();
		setErrorMsg(false);
		setSuccessMsg(false);
		
		final Income inc = buildIncome();

		try {
			
			addMsg(getIncomeService().addIncome(inc));
			setSuccessMsg(true);
		} catch (BusinessException e) {
			setMsg(e.getMsg());
			setErrorMsg(true);
		}

		if (getDateIni() == null) {
			setDateIni(new Date());
		}
		setDateFin(new Date());
		loadIncomes();
		resetFields();
		return "";
	}

	public Income buildIncome() {
		final Income inc = new Income();
		inc.setUserId(getUserId());
		inc.setIncomeTypeId(getIncomeType());
		inc.setDate(new Date());
		inc.setValue(getValue());
		return inc;
	}

	public boolean isSavedIncomeValid() {
		return getOutputMsg().contains("Erro");
	}

	public String deleteDialog() {
		return deleteAction(getSelectIncome());
	}

	public String deleteAction(Income income) {
		clearMsg();
		setErrorMsg(false);
		setSuccessMsg(false);
		try {
			
			addMsg(getIncomeService().deleteIncome(income));
			setSuccessMsg(true);
		} catch (BusinessException e) {
			setMsg(e.getMsg());
			setErrorMsg(true);
		}
		loadIncomes();
		return "";
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getIncomeType() {
		return incomeType;
	}

	public void setIncomeType(int incomeType) {
		this.incomeType = incomeType;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIncomeTypeDes() {
		return incomeTypeDes;
	}

	public void setIncomeTypeDes(String incomeTypeDes) {
		this.incomeTypeDes = incomeTypeDes;
	}

	public double getTotalExpenses() {
		return totalExpenses;
	}

	public void setTotalExpenses(double totalExpenses) {
		this.totalExpenses = totalExpenses;
	}

	public double getTotalIncomes() {
		return totalIncomes;
	}

	public void setTotalIncomes(double totalIncomes) {
		this.totalIncomes = totalIncomes;
	}

	public List<Income> getIncomeUserList() {
		// loadIncomes();
		return incomeUserList;
	}

	public void setIncomeUserList(List<Income> incomeUserList) {
		this.incomeUserList = incomeUserList;
	}

	public IncomeService getIncomeService() {
		return incomeService;
	}

	public void setIncomeService(IncomeService incomeService) {
		this.incomeService = incomeService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Date getDateIni() {
		return dateIni;
	}

	public void setDateIni(Date dateIni) {
		this.dateIni = dateIni;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public long getIncomeId() {
		return incomeId;
	}

	public void setIncomeId(long incomeId) {
		this.incomeId = incomeId;
	}

	public boolean isRenderPanel() {
		return renderPanel;
	}

	public void setRenderPanel(boolean renderPanel) {
		this.renderPanel = renderPanel;
	}

	public String getOutputLabel() {
		return outputLabel;
	}

	public void setOutputLabel(String outputLabel) {
		this.outputLabel = outputLabel;
	}

	public String getOutputMsg() {
		return outputMsg;
	}

	public void setOutputMsg(String outputMsg) {
		this.outputMsg = outputMsg;
	}

	public boolean isSuccessMsg() {
		return successMsg;
	}

	public void setSuccessMsg(boolean successMsg) {
		this.successMsg = successMsg;
	}

	public boolean isErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(boolean errorMsg) {
		this.errorMsg = errorMsg;
	}

	public List<User> getListUsers() {
		return listUsers;
	}

	public void setListUsers(List<User> listUsers) {
		this.listUsers = listUsers;
	}

	public Income getSelectIncome() {
		return selectIncome;
	}

	public void setSelectIncome(Income selectIncome) {
		this.selectIncome = selectIncome;
	}

	public List<String> getMsg() {
		return msg;
	}

	public void setMsg(List<String> msg) {
		final List<String> list = new ArrayList<String>();
		for(String value: msg){
			list.add(rb.getString(value));
		}
		this.msg = list;
	}

	public void addMsg(String msg) {
		this.msg.add(msg);
	}
	
	private void clearMsg(){
		try{
			this.msg.clear();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
