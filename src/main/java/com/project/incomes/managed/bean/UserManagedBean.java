package main.java.com.project.incomes.managed.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import main.java.com.project.incomes.model.User;
import main.java.com.project.incomes.service.UserService;
import main.java.com.project.incomes.service.impl.BusinessException;

import org.springframework.dao.DataAccessException;


@ManagedBean(name="userMB")
@RequestScoped
public class UserManagedBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String SUCCESS = "success";
	private static final String ERROR = "error";
	
	//private int id;
	private String name;
	private String email;
	private List<String> msg = new ArrayList<String>();
	private ResourceBundle rb = ResourceBundle.getBundle("bundle.message");

	@ManagedProperty(value="#{userServiceImpl}")
	UserService userService;
	
	List<User> userList;
	
	public UserService getUserService() {
		return userService;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public String addUser(){
		clearMsg();
		try{
			User user = new User();
			//user.setId(getId());
			user.setName(getName());
			user.setEmail(getEmail());
			try{
				getUserService().addUser(user);
			}
			catch(BusinessException e){
				setMsg(e.getMsg());
				return "";
			}
			return SUCCESS;
		}
		catch(DataAccessException ex){
			ex.toString();
			return ERROR;
		}
	}
	
	public void reset(){
		//this.setId(0);
		this.setName("");
		this.setEmail("");
	}
	
	@PostConstruct
	public void init(){
		userList = new ArrayList<User>();
		userList.addAll(getUserService().getUsers());
	}
	
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<String> getMsg() {
		return msg;
	}
	public void setMsg(List<String> msg) {
		final List<String> list = new ArrayList<String>();
		for(String value: msg){
			list.add(rb.getString(value));
		}
		this.msg = list;
	}

	private void clearMsg(){
		try{
			msg.clear();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
}
