package main.java.com.project.incomes.managed.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import javax.faces.bean.SessionScoped;

import main.java.com.project.incomes.model.Income;
import main.java.com.project.incomes.service.IncomeService;
import main.java.com.project.incomes.service.UserService;

import org.springframework.dao.DataAccessException;

@ManagedBean(name = "incomeMB")
@SessionScoped
public class IncomeManagedBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long userId;
	private int incomeType;
	private double value;
	private String userName;
	private String incomeTypeDes;
	private double totalValue;

	public double getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(double totalValue) {
		this.totalValue = totalValue;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIncomeTypeDes() {
		return incomeTypeDes;
	}

	public void setIncomeTypeDes(String incomeTypeDes) {
		this.incomeTypeDes = incomeTypeDes;
	}

	List<Income> incomeList;
	List<Income> incomeUserList;

	@ManagedProperty(value = "#{incomeServiceImpl}")
	IncomeService incomeService;
	@ManagedProperty(value = "#{userServiceImpl}")
	UserService userService;

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public List<Income> getIncomeList() {
		return incomeList;
	}

	public void setIncomeList(List<Income> incomeList) {
		this.incomeList = incomeList;
	}

	public String addInvoice() {
		try {
			final Income income = new Income();
			income.setUserId(getUserId());
			income.setIncomeTypeId(getIncomeType());
			income.setDate(new Date());
			income.setValue(getValue());
			//incomeService.addIncome(income);
			return "Success";
		} catch (DataAccessException e) {
			e.printStackTrace();
			return "Error";
		}

	}

	/*
	 * @PostConstruct public void init(){ loadIncomes(); }
	 */

	public void loadIncomes() {

		incomeUserList = new ArrayList<Income>();
		incomeUserList.addAll(getIncomeService().getIncomesByUser(getUserId()));
		totalValue = 0;
		for (Income inc : incomeUserList) {
			totalValue = totalValue + inc.getValue();
			inc.setUserName(getUserService().getUserById((int) getUserId()).getName());
			if (inc.getIncomeTypeId() == 1) {
				inc.setIncomeTypeDesc("Entrada");
			} else {
				inc.setIncomeTypeDesc("Saida");
			}
		}
		System.out.println(getUserId());
	}

	/*
	 * public void loadIncomes(long userId) {
	 * 
	 * incomeUserList = new ArrayList<Income>();
	 * incomeUserList.addAll(getIncomeService().getIncomesByUser(userId));
	 * System.out.println(getUserId()); }
	 */
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getIncomeType() {
		return incomeType;
	}

	public void setIncomeType(int incomeType) {
		this.incomeType = incomeType;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public List<Income> getIncomeUserList() {
		loadIncomes();
		return incomeUserList;
	}

	public void setIncomeUserList(List<Income> incomeUserList) {
		this.incomeUserList = incomeUserList;
	}

	public IncomeService getIncomeService() {
		return incomeService;
	}

	public void setIncomeService(IncomeService incomeService) {
		this.incomeService = incomeService;
	}

}
