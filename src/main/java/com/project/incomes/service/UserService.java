package main.java.com.project.incomes.service;

import java.util.List;

import main.java.com.project.incomes.model.User;
import main.java.com.project.incomes.service.impl.BusinessException;


public interface UserService {

	 /**
	  * Add User
	  *
	  * @param  User user
	 * @throws BusinessException 
	  */
	 public void addUser(User user) throws BusinessException;

	 /**
	  * Update User
	  *
	  * @param  User user
	  */
	 public void updateUser(User user);

	 /**
	  * Delete User
	  *
	  * @param  User user
	  */
	 public void deleteUser(User user);

	 /**
	  * Get User
	  *
	  * @param  int User Id
	  */
	 public User getUserById(int id);

	 /**
	  * Get User List
	  *
	  * @return List - User list
	  */
	 public List<User> getUsers();
	
}
