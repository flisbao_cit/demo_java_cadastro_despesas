package main.java.com.project.incomes.service;

import java.util.Date;
import java.util.List;

import main.java.com.project.incomes.model.Income;
import main.java.com.project.incomes.service.impl.BusinessException;


public interface IncomeService {

	/**
	  * Add User
	  *
	  * @param  User user
	 * @return 
	 * @throws BusinessException 
	  */
	 public String addIncome(Income income) throws BusinessException;

	 /**
	  * Update User
	  *
	  * @param  User user
	 * @throws BusinessException 
	  */
	 public String updateIncome(Income income) throws BusinessException;

	 /**
	  * Delete User
	  *
	  * @param  User user
	 * @throws BusinessException 
	  */
	 public String deleteIncome(Income income) throws BusinessException;

	 /**
	  * Get User
	  *
	  * @param  int User Id
	  */
	 public Income getIncomeById(int id);
	 
	 public List<Income> getIncomesByUser(long l);

	 /**
	  * Get User List
	  *
	  * @return List - User list
	  */
	 public List<Income> getIncomes();
	 public List<Income> getIncomesByDate(Date dtIni, Date dtFin, long userId);
}
