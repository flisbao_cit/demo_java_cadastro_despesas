package main.java.com.project.incomes.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import main.java.com.project.incomes.model.Income;
import main.java.com.project.incomes.repo.IncomeDao;
import main.java.com.project.incomes.service.IncomeService;

@Transactional(readOnly = true)
public class IncomeServiceImpl implements IncomeService {

	IncomeDao incomeDao;

	private List<String> msg = new ArrayList<String>();

	public IncomeDao getIncomeDao() {
		return incomeDao;
	}

	public void setIncomeDao(IncomeDao incomeDao) {
		this.incomeDao = incomeDao;
	}

	@Transactional(readOnly = true)
	public String addIncome(Income income) throws BusinessException {
		if (isValid(income, "add")) {
			getIncomeDao().addIncome(income);
			return "Adicionado com Sucesso!";

		} else {
			throw new BusinessException(this.msg);
		}

	}

	@Transactional(readOnly = true)
	public String updateIncome(Income income) throws BusinessException {
		// TODO Auto-generated method stub

		if (isValid(income, "update")) {
			getIncomeDao().updateIncome(income);
			return "Atualizado com Sucesso!";
		} else {
			throw new BusinessException(this.msg);
		}

	}

	@Transactional(readOnly = true)
	public String deleteIncome(Income income) throws BusinessException {
		// TODO Auto-generated method stub

		if (isValid(income, "delete")) {
			getIncomeDao().deleteIncome(income);
			return "Deletado com Sucesso!";
		} else {
			throw new BusinessException(this.msg);
		}

	}

	public Income getIncomeById(int id) {
		// TODO Auto-generated method stub
		return getIncomeDao().getIncomeById(id);
	}

	public List<Income> getIncomesByUser(long userId) {
		// TODO Auto-generated method stub
		return getIncomeDao().getIncomesByUser(userId);
	}

	public List<Income> getIncomes() {
		// TODO Auto-generated method stub
		return getIncomeDao().getIncomes();
	}

	public List<Income> getIncomesByDate(Date dtIni, Date dtFin, long userId) {
		return getIncomeDao().getIncomesByDate(dtIni, dtFin, userId);
	}

	private boolean isValid(Income income, String action) {
		boolean isValid = true;

		this.msg.clear();
		if (action == "add" || action == "update") {
			if (income.getDate().toString().isEmpty()) {
				isValid = false;
				msg.add("campo_data");
			}
			if (income.getUserId() <= 0) {
				isValid = false;
				msg.add("campo_usuario");
			}

			if (income.getValue() <= 0) {
				isValid = false;
				msg.add("campo_valor");
			}

			if (income.getIncomeTypeId() <= 0) {
				isValid = false;
				msg.add("campo_tipo");
			}
		} else {
			if (income.getIncomeId() < 0) {
				isValid = false;
				msg.add("campo_income_id");
			}
		}

		return isValid;
	}

}
