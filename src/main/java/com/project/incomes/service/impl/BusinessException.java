package main.java.com.project.incomes.service.impl;

import java.util.List;

public class BusinessException extends Exception {

	private static final long serialVersionUID = 1L;
	private List<String> msg;

	public List<String> getMsg() {
		return msg;
	}

	public void setMsg(List<String> msg) {
		this.msg = null;
		this.msg = msg;
	}

	public void addMsg(String msg) {
		this.msg.add(msg);
	}
	
	BusinessException(List<String> msg){
		setMsg(msg);
	}

}
