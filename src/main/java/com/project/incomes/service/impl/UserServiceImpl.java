package main.java.com.project.incomes.service.impl;

import java.util.ArrayList;
import java.util.List;

import main.java.com.project.incomes.model.User;
import main.java.com.project.incomes.repo.UserDao;
import main.java.com.project.incomes.service.UserService;

import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
	UserDao userDao;
	
	private List<String> msg = new ArrayList<String>();
	
	@Transactional(readOnly = true)
	//@Override
	public void addUser(User user) throws BusinessException {
		// TODO Auto-generated method stub
		
		if(isValid(user, "add")){
			getUserDao().addUser(user);
		}
		else{
			throw new BusinessException(msg);
		}
		
	}
	
	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Transactional(readOnly = true)
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		getUserDao().updateUser(user);
	}

	@Transactional(readOnly = true)
	public void deleteUser(User user) {
		// TODO Auto-generated method stub
		getUserDao().deleteUser(user);
	}

	public User getUserById(int id) {
		// TODO Auto-generated method stub
		return getUserDao().getUserById(id);
	}
	
	private List<User> getUserByNameAndEmail(String name, String email){
		return getUserDao().getUsersByNameAndEmail(name, email);
	}

	public List<User> getUsers() {
		// TODO Auto-generated method stub
		return getUserDao().getUsers();
	}
	
	private boolean isValid(User user, String action) {
		boolean isValid = true;
		this.msg.clear();
		final List<User> list = getUserByNameAndEmail(user.getName(), user.getEmail()); 
		if (action == "add" || action == "update") {
			if (user.getName().isEmpty()) {
				isValid = false;
				msg.add("campo_user_empty");
			}
			if (user.getEmail().isEmpty()) {
				isValid = false;
				msg.add("campo_usuario");
			}
			
			if(list.size()>0){
				isValid=false;
				msg.add("campo_nome_duplicado");
			}
		}
		return isValid;
	}

	
}
